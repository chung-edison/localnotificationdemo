Puede encontrar el tutorial aquí: [HOW TO MAKE LOCAL NOTIFICATIONS IN IOS 10](https://makeapppie.com/2016/08/08/how-to-make-local-notifications-in-ios-10/)

* Se utiliza el framework UserNotifications.
* Para utilizar las notificaciones es necesario obtener el permiso respectivo.
* La notificación tiene un título, subtítulo, y un cuerpo.
* Se puede implementar un disparador (en este caso se esperan 10 segundos antes de lanzar la notificación).
* Para lanzar la notificación, se utiliza un UNNotificationRequest, el cual es agregado al centro de notificaciones UNUserNotificationCenter.